README alexandre jallade

Fonctionnalités ajoutées:

Ajout du DOWNTO dans la boucle FOR (entièrement fonctionnel):
-permet d'avoir un boucle qui se décrémente.
grammaire avec: DOWNTO

Ajout d'un SWITCHCASE(entièrement fonctionnel):
-exactement le même role quand c++.
grammaire avec : SWITCHCASE, CASE, END.

Ajout des types pointeurs (int et bool) (fonctionnel mais limité):
-permet d'avoir un objet qui pointe vers un autre. (il contient donc son addresse)
utilisation:
    - [a,c] : INTP; (ou BOOLP) --> declaration variable de type pointeur
    - a->c ;                   --> fait pointer la variable a sur c
    - a->((5*9)+6);            --> fait pointer la variable a sur le resultat de l'operation
    - i=a!+1;                  --> effectue l'operation ou "a" est une variable de type pointeur. L'ajout du "!" signifie qu'on prend la valeur pointée par celle ci et non l'addresse.
    
    exemple:
        [a,b,c] : INT;
        [m,n,p] : INTP;
        b:=0;
        a:=0;
        m->a;
        b:=5;
        p->m!+b.
    
    problème:
        Si lors d'une operation la variable de type pointeur pointe elle meme vers une autre variable de type pointeur, le code n'affiche pas d'erreur (alors qu'il devrait) et affectue l'operation (illogique de faire une operation avec une adresse).
        
        exemple:
            [a,b,c] : INTP;
            [k,v] : INT;
            k:=5;
            a->k;
            b->c;
            v:=a!*b!;   --> impossible car "b" ne pointe pas vers un réel

Ajout du CIN(non fonctionnelle):
    -même Fonctionnalitée que le c++.
        grammaire avec: CIN
        probleme:
            La fonctionallité cin(dans mon compilateur.cpp) ne fonctionne pas.
