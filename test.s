			# This code was produced by the CERI Compiler
	.data
	.align 8
FormatString1:    .string "%llu\n"
a:	.quad 0
b:	.quad 0
c:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
m:	.quad 0
n:	.quad 0
p:	.quad 0
	push $0
	pop b
	push $0
	pop a
	push a
	pop %rax
	movq %rax, %rsi
	push %rsi
	pop m
	push $5
	pop b
	push m
	pop %rsi
	movq (%rsi), %rax
	push %rax
	push b
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop %rax
	movq %rax, %rsi
	push %rsi
	pop p
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
